package org.example.advice;


import io.grpc.Metadata;
import io.grpc.Status;
import io.grpc.StatusException;
import net.devh.boot.grpc.server.advice.GrpcAdvice;
import net.devh.boot.grpc.server.advice.GrpcExceptionHandler;
import org.example.advice.exception.UserNotFoundException;

@GrpcAdvice
public class GRpcExceptionAdvice {

    @GrpcExceptionHandler
    public Status handleInvalidArgument(IllegalArgumentException e) {
        return Status
                .INVALID_ARGUMENT
                .withDescription("Some message informing the client about the error.").withCause(e);
    }

    @GrpcExceptionHandler(UserNotFoundException.class)
    public StatusException handleUserNotFoundException(UserNotFoundException e) {
        Status status = Status
                .NOT_FOUND
                .withDescription("Some message informing the client about the error.")
                .withCause(e);

        return status
                .asException(buildMetadata());
    }

    Metadata buildMetadata() {
        Metadata metadata = new Metadata();

        return metadata;
    }
}
