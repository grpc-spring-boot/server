package org.example.converter;

import com.example.grpc.service.UserServiceOuterClass.UserRequest;
import org.example.model.User;
import org.springframework.stereotype.Component;

import java.util.HashSet;

@Component
public class UserGRpcConverter {

    public User toEntity(UserRequest userRequest) {
        return new User()
                .name(userRequest.getName())
                .surname(userRequest.getSurname())
                .hobbies(new HashSet<>(userRequest.getHobbiesList()));
    }
}
