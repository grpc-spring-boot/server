package org.example.interseptor;


import com.google.common.base.Stopwatch;
import io.grpc.ForwardingServerCallListener;
import io.grpc.Metadata;
import io.grpc.ServerCall;
import io.grpc.ServerCallHandler;
import io.grpc.ServerInterceptor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor;
import org.slf4j.MDC;
import org.springframework.core.Ordered;

import java.util.UUID;
import java.util.concurrent.TimeUnit;

@Slf4j
@GrpcGlobalServerInterceptor
public class GRpcMetricInterceptor implements ServerInterceptor, Ordered {

    private static final String DURATION = "duration";

    private final int order = 1;

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> call, Metadata headers, ServerCallHandler<ReqT, RespT> next) {
        MDC.put("requestKey", UUID.randomUUID().toString());

        return new ServerListener<>(next.startCall(call, headers), call);
    }

    @Override
    public int getOrder() {
        return this.order;
    }

    private static class ServerListener<ReqT, RespT> extends ForwardingServerCallListener<ReqT> {
        private final ServerCall.Listener<ReqT> delegate;
        private final ServerCall<ReqT, RespT> call;
        private final Stopwatch stopwatch;

        ServerListener(ServerCall.Listener<ReqT> delegate, ServerCall<ReqT, RespT> call) {
            this.stopwatch = Stopwatch.createStarted();
            this.delegate = delegate;
            this.call = call;
        }

        @Override
        public void onComplete() {
            String methodName = call.getMethodDescriptor().getFullMethodName();
            final long elapsed = stopwatch.elapsed(TimeUnit.MILLISECONDS);

            MDC.put(DURATION, Long.toString(elapsed));
            log.info("{} call completed in {} ms", methodName, elapsed);
            MDC.clear();
        }

        @Override
        protected ServerCall.Listener<ReqT> delegate() {
            return delegate;
        }
    }

    @Override
    public String toString() {
        return GRpcMetricInterceptor.class.getSimpleName() + " [order = " + this.order + "]";
    }
}
