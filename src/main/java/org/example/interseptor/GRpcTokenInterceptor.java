package org.example.interseptor;

import io.grpc.*;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.interceptor.GrpcGlobalServerInterceptor;
import org.example.service.JWTVerifierService;
import org.example.util.Constant;
import org.springframework.core.Ordered;

import java.util.Map;

import static java.util.Objects.isNull;


@Slf4j
@AllArgsConstructor
@GrpcGlobalServerInterceptor
public class GRpcTokenInterceptor implements ServerInterceptor, Ordered {

    private final int order = 0;

    private final JWTVerifierService verifier;

    @Override
    public <ReqT, RespT> ServerCall.Listener<ReqT> interceptCall(ServerCall<ReqT, RespT> serverCall, Metadata metadata, ServerCallHandler<ReqT, RespT> serverCallHandler) {
        String jwt = metadata.get(Constant.JWT_METADATA_KEY);
        if (isNull(jwt)) {
            serverCall.close(Status.UNAUTHENTICATED.withDescription("JWT Token is missing from Metadata"), metadata);
            return new ServerCall.Listener<ReqT>() {
            };
        }

        Context ctx;
        try {
            Map<String, Object> verified = verifier.verify(jwt);
            ctx = Context.current()
                    .withValue(Constant.USER_ID_CTX_KEY, verified.getOrDefault("sub", "anonymous").toString())
                    .withValue(Constant.JWT_CTX_KEY, jwt);
        } catch (Exception e) {
            log.warn("Verification failed - Unauthenticated!");
            serverCall.close(Status.UNAUTHENTICATED.withDescription(e.getMessage()).withCause(e), metadata);
            return new ServerCall.Listener<ReqT>() {
            };
        }

        return Contexts.interceptCall(ctx, serverCall, metadata, serverCallHandler);
    }

    @Override
    public int getOrder() {
        return this.order;
    }

    @Override
    public String toString() {
        return GRpcTokenInterceptor.class.getSimpleName() + " [order = " + this.order + "]";
    }
}
