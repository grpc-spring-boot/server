package org.example.repo;


import org.example.model.User;
import org.springframework.stereotype.Service;

import java.util.Map;
import java.util.Optional;
import java.util.Random;
import java.util.UUID;
import java.util.concurrent.ConcurrentHashMap;

@Service
public class UserRepository {
    private Map<String, User> userIdToUser = new ConcurrentHashMap<>();


    public Optional<String> save(User user) {
        String id = UUID.randomUUID().toString();

        userIdToUser.put(id, user);

        return new Random().nextBoolean()
                ? Optional.of(id)
                : Optional.empty();
    }
}
