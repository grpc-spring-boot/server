package org.example.service;

import com.example.grpc.service.UserServiceGrpc.UserServiceImplBase;
import com.example.grpc.service.UserServiceOuterClass.UserRequest;
import com.example.grpc.service.UserServiceOuterClass.UserResponse;
import io.grpc.Status;
import io.grpc.StatusRuntimeException;
import io.grpc.stub.StreamObserver;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.devh.boot.grpc.server.service.GrpcService;
import org.example.converter.UserGRpcConverter;
import org.example.model.User;
import org.example.repo.UserRepository;

import java.util.Optional;


@Slf4j
@AllArgsConstructor
@GrpcService
public class UserServiceImp extends UserServiceImplBase {

    private final UserRepository userRepository;
    private final UserGRpcConverter userConverter;

    @Override
    public void saveUnary(UserRequest request, StreamObserver<UserResponse> responseObserver) {

        try {
            final User user = userConverter.toEntity(request);
            final Optional<String> userUUid = userRepository.save(user);

            if (userUUid.isPresent()) {
                UserResponse userResponse = UserResponse.newBuilder()
                        .setId(userUUid.get())
                        .build();

                responseObserver.onNext(userResponse);
                responseObserver.onCompleted();
            } else {
                responseObserver.onError(new StatusRuntimeException(Status.INVALID_ARGUMENT
                        .withDescription("Some message informing the client about the error.")));
            }
        } catch (Exception ex) {
            responseObserver.onError(new StatusRuntimeException(Status.INTERNAL
                    .withDescription("Something bad happened: " + ex.getMessage())));
        }
    }

    @Override
    public StreamObserver<UserRequest> saveBidirectionalStreaming(StreamObserver<UserResponse> responseObserver) {

        return super.saveBidirectionalStreaming(responseObserver);
    }

    @Override
    public void saveClientStreaming(UserRequest request, StreamObserver<UserResponse> responseObserver) {
        super.saveClientStreaming(request, responseObserver);
    }

    @Override
    public StreamObserver<UserRequest> saveServerStreaming(StreamObserver<UserResponse> responseObserver) {
        return super.saveServerStreaming(responseObserver);
    }
}


