package org.example.util;


import io.grpc.Context;
import io.grpc.Metadata;

import static io.grpc.Metadata.ASCII_STRING_MARSHALLER;


public class Constant {

    public static final Context.Key<String> USER_ID_CTX_KEY = Context.key("userId");
    public static final Context.Key<String> JWT_CTX_KEY = Context.key("jwt");
    public static final Metadata.Key<String> JWT_METADATA_KEY = Metadata.Key.of("jwt", ASCII_STRING_MARSHALLER);
}
